import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api.zidus.interact.tec.br/',
});

export default api;