import React, { useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom';

import { CointanerVideo, Video, Midia, BackButton } from './style';

function VideoAdmin() {
  const history = useHistory();
  const token = localStorage.getItem('token');
  const userToken = localStorage.getItem('role');

  useEffect(() => {
    if (token === null) {
      history.push('/');
    } else {
      userToken !== 'admin' ? history.push('video/') : history.push('videoadmin/');
    }
  }, []);
  return (
    <CointanerVideo>
      <Video>
        <Midia />
        <Link to="/painel" style={{ textDecoration: "none" }}>
          <BackButton>VOLTAR</BackButton>
        </Link>
      </Video>
    </CointanerVideo>
  );
}

export default VideoAdmin;