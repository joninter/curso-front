import styled from 'styled-components';

export const CointanerVideo = styled.div`
  background-color: #144C91;
  width: 100%;
  height: 100%;
`;

export const Video = styled.div`
height: 90%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Midia = styled.div`
  align-items: center;
  justify-content: center;
  background-color: #1C1C1B;
  width: 1166.34px;
  height: 641.6px;
`

export const BackButton = styled.button`
  background-color:  rgb(141,198,62);
  color: white;
  width: 111.54px;
  height: 35.84px;
  align-items: center;
  display: flex;
  margin-top:17px;
  align-items: center;
  justify-content: center;
`;