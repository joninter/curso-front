import styled from 'styled-components';

export const ContainerCross = styled.button`
position: absolute;
margin-top: -38px;
    margin-left: 276px;
background: transparent;
border: 0;
padding: 5px;
cursor: "pointer" 

`

export const Img = styled.img`
width: 20px;

`


export const BackButton = styled.button`
  background-color:  rgb(141,198,62);
  color: white;
  width: 111.54px;
  height: 35.84px;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 300px;
  margin-top: -86px;

  @media(max-width: 1366px) {
    background-color: rgb(141,198,62);
    color: #fff;
    width: 111.54px;
    height: 35.84px;
    position: absolute;
    margin: auto;
    margin-top: -55px;
    margin-left: 329px;
   }
`;

export const ActiveQuiz = styled.button`
  background-color:  rgb(141,198,62);
  color: white;
  width: 111.54px;
  height: 35.84px;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 150px;

 
  @media(min-width: 780px) {
    background-color: rgb(141,198,62);
    color: #fff;
    width: 111.54px;
    height: 35.84px;
    position: absolute;
    margin: auto;
    margin-left: 40%;
   }
`;

export const EnviarResposta = styled.button`
    background-color: rgb(141,198,62);
    display: flex;
    height: 26px;
    width: 64px;
    text-align: center;
    justify-content: center;
    padding-top: 0;
    margin-top: 5%;
    margin-left: 25%;
    color:white;
`

export const ActiveResultQuiz = styled.button`
  background-color:  rgb(141,198,62);
  color: white;
  width: 111.54px;
  height: 35.84px;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 5px;

  @media(min-width: 780px) {
    background-color: rgb(141,198,62);
    color: #fff;
    width: 111.54px;
    height: 35.84px;
    position: absolute;
    margin: auto;
   }
`;

export const Logo = styled.img`
  width: 250px;
  margin-left: 30px;

  @media(max-width: 1366px) {
    width: 278px;
    margin-top: -16px;
    margin-left: 30px;
   }
`;

export const Porcent = styled.div`
 color:white;
 width: 50px;
 height: 150px;
 text-align: center;
 position: absolute;
 margin-top: -40px;
 margin-left: 105px;

`

export const Questions = styled.div`
  width: 311.13px;
  height:527.79px;
  background-color: #144C91;
  color:white;
  text-transform: uppercase;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  margin-left: 10px;
  margin-right: 5px;

  h1{
    font-size: 22px; 
  }
  p{
    font-size: 13px;
  }

  @media(max-width: 1366px) {
    width: 311.13px;
    height:527.79px;
    background-color: #144C91;
    color:white;
    text-transform: uppercase;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: absolute;
    margin-left: 23px;

    h1{
      margin-top: 20px;
      font-size: 22px; 
    }

    p{
      margin-left: 10px;
      font-size: 13px;
    }
  }
`;

export const Quizes = styled.div`
  display: grid;
  grid-template-columns: repeat(4,6fr);
  gap: 10px;
  height: 217.52px;
  width: 311.13px;
  margin-left: 30px;
  background-color: #144C91;
  

  h1{
    font-size: 20px;
  }

  @media(min-width: 780px) {
    display: grid;
    grid-template-columns: repeat(4,6fr);
    gap: 10px;
    height: 217.52px;
    width: 311.13px;
    margin-left: 60px;
    background-color: #144C91;

  h1{
    font-size: 20px;
  }
   }
`;

export const ContainerGrid = styled.div`
  position: absolute;
  margin-left: 300px;
`

export const Quiz = styled.div`
  padding-top: 12px;
  height: 217.52px;
  color: white;
  width: 311.13px;
  background-color: #144C91;
  
  h1{
    margin-left: 10px;
  }

  p{
    margin-left: 10px;
  }
`;
export const Result = styled.div`
    border-top: 3px white solid;
    background-color: #144C91;
    color: white;
    padding-left: 10px;
    margin-top: 215px;
    width: 311px;
    margin-left: -321px;
    height: 46px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-align-items: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;

    @media(min-width: 1366px) {
      border-top: 3px white solid;
    background-color: #144C91;
    color: white;
    padding-left: 10px;
    margin-top: 217px;
    width: 311px;
    margin-left: -321px;
    height: 46px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-align-items: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
  
   }
`;

export const ContainerButton = styled.div`
  position: absolute;
  margin-left: 1015px;
  padding-top: 12px;
  height: 217.52px;
  color: white;
  width: 311.13px;


  @media(max-width: 1366px){
    position: absolute;
    margin-left: 1031px;
    padding-top: 12px;
    height: 217.52px;
    color: white;
    width: 311.13px;
  }
`;

export const BigButton = styled.button`
  width: 311.13px;
  height: 216.52px;
  margin-top: -11px;
  background-color: #144C91;
  border-radius: 0;
  margin-bottom: 33px;
  color: white;
`;

export const BigResposta = styled.div`
    width: 311.13px;
    height: 216.52px;
    margin-top: -11px;
    background-color: #144C91;
    border-radius: 0;
    margin-bottom: 33px;
    color: white;
    justify-content: center;
    display: flex;
    align-items: center;

    input{
      margin-bottom: 3px;
    }

  button{
    background-color: rgb(141,198,62);
  }
`;

export const BigButton2 = styled.button`
 padding-top: 12px;
    height: 217.52px;
    color: white;
    width: 311.13px;
    margin-top: 22px;
    border-radius: 0;
    background-color: #144C91;

    button{
      background-color: rgb(141,198,62);
    display: flex;
    padding: 0;
    margin: auto;
    margin-top: 10px;
    width: 55px;
    justify-content: center;
    }
`;

export const SmallButton = styled.button`
    background-color: #144C91;
    border-radius: 0;
    height: 46px;
    width: 311px;
    border-top: 3px white solid;
    color: white;
    margin-top: -3px;
`;

export const Operator = styled.div`
    background-color: rgb(141,198,62);
    margin-top: 9px;
    border-radius: 7px;

    p{
      padding-left: 20px;
    }
`;