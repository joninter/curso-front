import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom';
import { MdClose } from "react-icons/md";
import { HiRefresh } from "react-icons/hi";

import api from '../../services/api'

import logo from '../../assets/logo.png'
import { BackButton, ActiveQuiz, ActiveResultQuiz, EnviarResposta, Logo, Img, ContainerCross, Questions, Quiz, Result, Quizes, Porcent, ContainerGrid, ContainerButton, BigButton, SmallButton, BigButton2, Operator, BigResposta } from './styles'
import compression from 'compression';

const steps = [
  {
    id: "PERGUNTA",
    title: "Digite a sua nova pergunta"
  },
  {
    id: "RESPOSTA",
    title: "Digite as opções de resposta"
  }
];

const Controle = (props) => {
  const [qa, setQa] = useState('');
  const [opt1, setOpt1] = useState('');
  const [opt2, setopt2] = useState('');
  const [opt3, setopt3] = useState('');
  const [opt4, setopt4] = useState('');
  const [textNotice, setTextNotice] = useState('')

  const [currentStep, setCurrentStep] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [notice, setNotice] = useState([]);
  const [quiz, setQuiz] = useState([]);
  const token = localStorage.getItem('token');
  const userToken = localStorage.getItem('role');
  const name = localStorage.getItem('name')
  const [enquete, setEnquete] = useState(false)
  const [enviarNotice, setEnviarNotice] = useState(false)
  const [activeQuiz, setActiveQuiz] = useState(false)
  const [results, setResults] = useState([])
  const dados = localStorage.getItem('result')

  useEffect(() => {
    getQuestion()
    getQuiz()
    getNotice()
  }, []);

  const history = useHistory();

  const header = {
    Authorization: `Bearer ${token}`,
  };

  function handleNext() {
    setCurrentStep((prevState) => prevState + 1);
  }

  const getQuestion = () => {
    api
      .get(`painel/qas`, { headers: header })
      .then((response) => {
        setQuestions(response.data)
      })
      .catch((err) => {
        console.log('erro');
      });
  }

  const getQuiz = () => {
    api
      .get(`painel/allquestions`, { headers: header })
      .then((response) => {
        const result = response.data
        const orderQuestion = result.sort(function (x, y) {
          return x.remove - y.remove
        })
        setQuiz(orderQuestion)
      })
      .catch((err) => {
        console.log('erro');
      });
  }


  const getNotice = () => {
    api
      .get(`painel/notices/`, { headers: header })
      .then((response) => {
        setNotice(response.data)
      })
      .catch((err) => {
        console.log('erro');
      });
  }


  const getResultQuiz = (id) => {
    const id_question = id
    api
      .get(`painel/answer/${id_question}`, { headers: header })
      .then((response) => {
        console.log('resul', results)
        const productIndex = results.findIndex((p) => p.id === id);
        console.log('index', productIndex)
        if (results !== -1) {
          console.log('meu index', results[productIndex])
          results[productIndex] = {
            id: id,
            a: response.data.a,
            b: response.data.b,
            c: response.data.c
          }
          setResults([...results]);
          results.splice(0, 1);
          localStorage.setItem("results", JSON.stringify(results));
        }
        const a = { id: id, a: response.data.a, b: response.data.b, c: response.data.c }
        setResults([...results, a])
      })
      .catch((err) => {
        console.log('erro');
      });
  }



  const handleRemoveQA = (id, visible) => {
    const visibleQA = !visible
    let body = {
      id_qa: id,
      visible: visibleQA
    };
    api
      .put(`painel/qas`, body, { headers: header })
      .then((response) => {
        console.log('response', response)
        getQuestion()
      })
      .catch((err) => {
        console.log('erro');
      });
  };


  useEffect(() => {
    const timer = setTimeout(() => getQuestion(), 5000);
    getQuiz()
    return () => clearTimeout(timer);
  }, [questions]);

  useEffect(() => {
    if (token === null) {
      history.push('/');
    } else {
      if (userToken !== 'admin') history.push('video/');
    }
  }, []);

  function ChangeQuestion(e) {
    setQa(e.target.value);
  }

  function ChangeOpt1(e) {
    setOpt1(e.target.value);
  }

  function ChangeOpt2(e) {
    setopt2(e.target.value);
  }

  function ChangeOpt3(e) {
    setopt3(e.target.value);
  }

  function ChangeOpt4(e) {
    setopt4(e.target.value);
  }

  function ChangeQA(e) {
    setTextNotice(e.target.value);
  }

  const createEnquete = () => {
    setEnquete(!enquete)
  }

  const createNotice = () => {
    setEnviarNotice(!enviarNotice)
  }

  async function handleSubmit(e) {
    e.preventDefault();
    let body = {
      question: qa,
      option1: opt1,
      option2: opt2,
      option3: opt3,
      option4: opt4,
    };
    setEnquete(!enquete)
    await api
      .post(`painel/questions`, body, { headers: header })
      .then((response) => {
        setCurrentStep(0);
        setEnquete(!enquete)
        setQa('')
        setOpt1('')
        setopt2('')
        setopt3('')
      })
      .catch((err) => {
        console.log('erro');
      });

    getQuiz()
  }

  async function handleActive(id, remove) {
    const active = !remove
    const body = {
      remove: active
    }
    await api
      .put(`painel/questions/${id}`, body, { headers: header })
      .then((response) => {
        getQuiz()
      })
      .catch((err) => {
        console.log('erro');
      });
  }

  async function handleVisibleOperator(id, visibleQuestionOperator) {
    const visible = !visibleQuestionOperator
    const remove = false
    let body = {
      id: id,
      visibleQuestionOperator: visible
    };

    await api
      .put(`painel/visibleOperator`, body, { headers: header })
      .then((response) => {
        handleActive(id, remove)
        getQuiz()
      })
      .catch((err) => {
        console.log('erro');
      });
  }

  async function handleVisibleAnswer(id, visibleAnswer) {
    const visible = !visibleAnswer
    let body = {
      id: id,
      visible: visible
    };
    await api
      .put(`painel/answervisible`, body, { headers: header })
      .then((response) => {
        getQuiz()
      })
      .catch((err) => {
        console.log('erro');
      });
  }


  async function RemoveNotice(id, active) {
    const visible = !active
    let body = {
      id_notice: id,
      active: visible
    };
    await api
      .put(`painel/notice`, body, { headers: header })
      .then((response) => {
        setNotice(body)
      })
      .catch((err) => {
        console.log('erro');
      });

    getNotice()

  }

  async function handleNotice(e) {
    e.preventDefault();
    let body = {
      notice: textNotice,
      active: true
    };
    setEnviarNotice(!enviarNotice)
    await api
      .post(`painel/notices`, body, { headers: header })
      .then((response) => {
        setNotice(body)
        setTextNotice('')
      })
      .catch((err) => {
        console.log('erro');
      });
    getNotice()
  }

  return (
    <>
      <Logo src={logo} alt="logo" />
      <Link to="/painel">
        <BackButton >VOLTAR</BackButton>
      </Link>
      <Questions>
        <h1> PERGUNTAS DA PLATEIA</h1>
        {questions.map(question => (
          <p key={question.id}>
            {question.question}
            <MdClose size={20} style={{ color: "red" }} onClick={() => handleRemoveQA(question.id, question.visible)} />
          </p>
        ))}
      </Questions>
      {quiz.length < 1 ? '' :
        <ContainerGrid>
          <Quizes>
            {quiz.map(p => (
              <>
                <Quiz key={p.id}>
                  <MdClose onClick={() => handleVisibleOperator(p.id, p.visibleQuestionOperator)} style={{
                    color: "red", float: "right", marginTop: "-12px"
                  }}
                    size={25}
                  />
                  <h1>{p.question}</h1>

                  {
                    console.log(p.id === results.id)
                  }
                  {
                    results.map((r) => (
                      <>
                        <p>A - {p.option1}</p><Porcent>{r.id === p.id && r.a !== null ? r.a + '%' : null}</Porcent>
                        <p>B - {p.option2}</p><Porcent>{r.id === p.id && r.b !== null ? r.b + '%' : null}</Porcent>
                        <p>C - {p.option3}</p><Porcent>{r.id === p.id && r.c !== null ? r.c + '%' : null}</Porcent>
                      </>
                    ))
                  }
                </Quiz>
                <Result >
                  <HiRefresh size={25} style={{ color: "white", marginLeft: "72%", position: "absolute" }} onClick={() => getResultQuiz(p.id)} />
                  {
                    p.remove !== true ? <></>
                      : <>
                        {p.visibleResponse === false ?
                          <ActiveResultQuiz onClick={() => handleVisibleAnswer(p.id, p.visibleResponse)}>RESULTADO</ActiveResultQuiz>
                          :
                          <ActiveResultQuiz onClick={() => handleVisibleAnswer(p.id, p.visibleResponse)}>OCULTAR</ActiveResultQuiz>
                        }
                      </>
                  }

                  {
                    p.visibleResponse === true ? <></>
                      :
                      <>
                        {p.remove !== true ?
                          <ActiveQuiz onClick={() => handleActive(p.id, p.remove)}>DESATIVAR</ActiveQuiz>
                          :
                          <ActiveQuiz onClick={() => handleActive(p.id, p.remove)}>ATIVAR</ActiveQuiz>

                        }
                      </>
                  }


                </Result>
              </>
            ))}
          </Quizes>
        </ContainerGrid>}

      <ContainerButton>

        {enquete === false ? <BigButton onClick={createEnquete}>CADASTRAR NOVA ENQUETE</BigButton>
          : <BigResposta>
            <form onSubmit="return false;" style={{ marginRight: "8%" }}>

              {steps[currentStep].id === "PERGUNTA" && (
                <>
                  <p style={{ textAlign: "center", marginRight: "0%" }}>QUAL A SUA PERGUNTA?</p>
                  <input value={qa} onChange={ChangeQuestion} style={{ width: "100%" }} />
                </>
              )}
              {steps[currentStep].id === "RESPOSTA" && (
                <div style={{
                  display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center",
                }}>
                  <p>SUA RESPOSTA?</p>
                  <input value={opt1} onChange={ChangeOpt1} />
                  <input value={opt2} onChange={ChangeOpt2} />
                  <input value={opt3} onChange={ChangeOpt3} />
                </div>
              )}

              {/* 
              
              
              */}
              {currentStep < steps.length - 1 && (
                <button type="button" style={{ color: "white", display: "flex", padding: "0", width: "100px", textAlign: "center", justifyContent: "center", marginLeft: "25%", marginRight: "50%" }} onClick={handleNext}>
                  PROXIMO
                </button>
              )}
              {currentStep === steps.length - 1 && (
                <EnviarResposta onClick={handleSubmit} style={{ width: "100px" }}>
                  ENVIAR
                </EnviarResposta>
              )}
            </form>

          </BigResposta>
        }
        {
          notice.length > 0 ?
            <BigButton2 >{notice[0].notice}</BigButton2> : <>{enviarNotice === false ?
              <BigButton2 onClick={createNotice}>ENVIAR NOVO AVISO</BigButton2> :
              <BigButton2>
                <form onSubmit={handleNotice}>
                  <p>ESCREVA SEU AVISO</p>
                  <input value={textNotice} onChange={ChangeQA} />
                  <button type="submit" style={{ color: 'white', width: "100px" }}>
                    ENVIAR
                  </button>
                </form>
              </BigButton2>
            }</>
        }{
          notice.length > 0 ?
            <SmallButton onClick={() => RemoveNotice(notice[0].id, notice[0].active)}>APAGAR NOTICIA ATUAL</SmallButton>
            :
            ''
        }
        <Operator><p>OPERADOR: {name}</p></Operator>
      </ContainerButton>
    </>
  );
};

export default Controle;