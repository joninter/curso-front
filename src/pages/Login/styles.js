import backgroundDesktop from '../../assets/wallpaper.png';
import backgroundMobile from '../../assets/loginMobile.jpg';
import styled from 'styled-components';

export const ContainerLoginPage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
  background-image: url(${backgroundMobile});
  background-size: cover;
  background-position: center center;

  @media (min-width: 769px) {
    padding-left: 15%;
    justify-content: flex-start;
    background-image: url(${backgroundDesktop});
    background-position: left center;
  }
`;

export const LoginDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  background-color: #0f4d92;
  border-radius: 20px;
  height: 250px;
  width: 270px;

  /* @media (width: 768px) {
    height: 450px;
    width: 420px;
  } */

  @media (min-width: 768px) {
    height: 450px;
    width: 420px;
  }

  @media (min-width: 1920px) {
    height: 550px;
    width: 520px;
  }
`;

export const Title = styled.div`
  font-size: clamp(20px, 3vw, 40px);
  color: #fff;
`;

export const LoginInput = styled.input`
  &::placeholder {
    color: #fff;
    opacity: 0.7;
  }

  width: 90%;
  height: 20%;
  background-color: transparent;
  border-color: #fff;
  border-radius: 10px;
  color: white;
  text-align: center;
  font-size: clamp(20px, 3vw, 30px);
`;

export const ButtonLogin = styled.div`
  font-size: clamp(20px, 3vw, 40px);
  color: #fff;
`;
