import React, { useState, useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom';
import api from '../../services/api';

import { ContainerLoginPage, LoginDiv, LoginInput, ButtonLogin, Title } from './styles';
import Logo from '../../assets/wallpaper.png'


function Login() {
  const [user, setUser] = useState('');
  const token = localStorage.getItem('token');
  const userToken = localStorage.getItem('role');


  const history = useHistory();

  useEffect(() => {
    if (token === null) {
      history.push('/');
    } else {
      if (userToken === 'user') {
        history.push('video/')
      } else {
        history.push('painel/')
      }
    }
  }, []);

  function Login(e) {
    setUser(e.target.value);
  }

  const body = {
    email: user,
  };

  async function handleLogin(e) {
    e.preventDefault();
    try {
      const response = await api.post('login', body);

      localStorage.setItem('token', response.data.token);
      localStorage.setItem('name', response.data.user.name)
      localStorage.setItem('role', response.data.user.role);
      if (response.data.user.role === 'user') {
        history.push('video/');
      } else {
        history.push('painel/');
      }
    } catch (err) {
      console.log('erro', err);
    }
  }

  return (
    <>
      <ContainerLoginPage>
        <div src={Logo} alt="test" />

        <form >
          <LoginDiv>
            <Title>Acesso</Title>
            <LoginInput value={user} onChange={Login} type="email" placeholder="E-MAIL ZYDUS" />
            <ButtonLogin type="submit" onClick={handleLogin}>Entrar</ButtonLogin>
          </LoginDiv>
        </form>
      </ContainerLoginPage>
    </>
  );
}

export default Login;