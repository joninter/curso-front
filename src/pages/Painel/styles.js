import styled from 'styled-components';
import img from '../../assets/background.jpg';

export const ContainerLoginPage = styled.body`
  flex-direction: column;
  display: flex;
  justify-content: center;
  align-items: center;
  background-image: url(${img});
  background-size: cover;
  width: 100vw;
  height: 100vh;
  color: white;
  background-color: #144C91;

  @media(max-width: 1366px) {
   im: url(${img});
   background-size: contain;
   width: 100vw;
   height: 100vh;
  }
`;

export const ButtonTransmissao = styled.button`
    background-color: rgb(141,198,62);
    color: #fff;
    width: 342.11px;
    height: 146.72px;
    position: absolute;
    margin: auto;
    margin-top: -149px;
    margin-left: -592px;

   @media(max-width: 1366px) {
      background-color: rgb(141,198,62);
    color: #fff;
    width: 342.11px;
    height: 146.72px;
    position: absolute;
    margin-top: -98px;
    margin-left: -446px;
   }
`;

export const ButtonEnquete = styled.button`
   background-color: rgb(141,198,62);
    color: #fff;
    width: 342px;
    height: 146px;
    position: absolute;
    margin: auto;
    margin-left: 136px;
    margin-top: -149px;

   @media(max-width: 1366px) {
      background-color: rgb(141,198,62);
    color: #fff;
    width: 425.11px;
    height: 146.72px;
    position: absolute;
    margin: auto;
    margin-top: -98px;
    margin-left: 131px;
   }
`;

export const Logo = styled.img`
  width: 481px;
    position: absolute;
    margin: auto;
    margin-left: 211.07px;
    margin-top: 13.26px;
   `

export const ButtonSair = styled.button`
 background-color: rgb(141,198,62);
    color: #fff;
    width: 156px;
    height: 50px;
    position: absolute;
    margin: auto;
    margin-top: 272px;
    margin-left: -61px;
   
   @media(max-width: 1366px) {
      background-color: rgb(141,198,62);
    color: #fff;
    width: 156.63px;
    height: 50.32px;
    position: absolute;
    margin: auto;
    margin-top: 171px;
    margin-left: -88px;
   }
`;

export const LoginDiv = styled.div`
  flex-direction: row;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 10%;
`;


export const LoginInput = styled.input`
display: block;
 width: 424.8px;
 height: 100.05px;
 background-color: #0A47E3;
 border-color: white;
 border-radius: 10px;
 color: white;
 ::-webkit-input-placeholder {
   color: white;
}

:-moz-placeholder { /* Firefox 18- */
   color: white;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: white;  
}

:-ms-input-placeholder {  
   color: white;  
}
`;

export const CointainerSair = styled.div`
margin-top: 20px;
`