import React, { useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom';
import logo from '../../assets/logo.png'
import { CointainerSair, Logo, ContainerLoginPage, ButtonSair, ButtonEnquete, ButtonTransmissao, LoginDiv } from './styles';




function Painel() {
  const history = useHistory();
  const token = localStorage.getItem('token');
  const userToken = localStorage.getItem('role');

  useEffect(() => {
    if (token === null) {
      history.push('/');
    }
  }, []);


  function handleLogout() {
    try {
      localStorage.removeItem('token');
      localStorage.removeItem('role');
      history.push('/');
    } catch (err) {
      console.log('erro ao deslogar');
    }
  }
  return (

    <>
      <Logo src={logo} alt="Logo" />
      <ContainerLoginPage>
        <LoginDiv>
          <Link to="/video">
            <ButtonTransmissao>VEJA A TRANSMISSAO</ButtonTransmissao>
          </Link>
          <Link to="/controle">
            <ButtonEnquete>VER PERGUNTAS / ENVIAR PERGUNTAS E ENQUETES</ButtonEnquete>
          </Link>
        </LoginDiv>
        <CointainerSair>
          <ButtonSair onClick={handleLogout}>SAIR</ButtonSair>
        </CointainerSair>
      </ContainerLoginPage>

    </>

  );
}

export default Painel;