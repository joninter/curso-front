import React, { useEffect, useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import api from '../../services/api';
import * as S from './styles';
import logo from '../../assets/logo.png';

function VideoUser() {
  const [quiz, setQuiz] = useState([]);
  const [resultQuiz, setResultQuiz] = useState([]);
  const [notice, setNotice] = useState([]);
  const [qa, setQa] = useState('');
  const [pergunta, setPergunta] = useState(false);
  const [votou, setVotou] = useState(false);
  const userToken = localStorage.getItem('role');
  const token = localStorage.getItem('token');
  const history = useHistory();
  const results = [];
  const responseQuiz = localStorage.getItem('id');
  const [resultsAnswer, setResultsAnswer] = useState([])

  const [logoCondition, setLogo] = useState(true);

  useEffect(() => {
    getQuiz();
    getNotice();
    getResponse();
  }, []);


  const handleResponseAnswer = (value, id) => {
    console.log('AQUI ENTROU!!');
    let body = {
      id_question: id,
      answer: value,
    };
    api
      .post(`course/question/`, body, { headers: header })
      .then((response) => {
        setVotou(true)
        getQuiz()
        localStorage.setItem('id', response.data.id_question);
        return results.push(response.data);
      })
      .catch((err) => {
        console.log('erro');
      });
  };

  const header = {
    Authorization: `Bearer ${token}`,
  };

  const getQuiz = () => {
    api
      .get(`painel/questions`, { headers: header })
      .then((response) => {
        setQuiz(response.data);
        if (responseQuiz === response.data[0].id || votou === true) {
          setLogo(true)
        } else {
          setLogo(false)
        }

      })
      .catch((err) => {
        console.log('erro');
      });
  };

  function handleLogout() {
    try {
      localStorage.removeItem('token');
      localStorage.removeItem('role');
      history.push('/');
    } catch (err) {
      console.log('erro ao deslogar');
    }
  }

  const getResponse = () => {
    api
      .get(`course/answervisible`, { headers: header })
      .then((response) => {
        setResultQuiz(response.data);
      })
      .catch((err) => {
        console.log('erro');
      });
  };
  const getNotice = () => {
    api
      .get(`painel/notices`, { headers: header })
      .then((response) => {
        setNotice(response.data);
      })
      .catch((err) => {
        console.log('erro');
      });
  };

  function ChangeQuestion(e) {
    setQa(e.target.value);
  }

  async function handleSubmit(e) {
    e.preventDefault();
    let body = {
      question: qa,
    };
    setPergunta(!pergunta);
    await api
      .post(`course/qa`, body, { headers: header })
      .then((response) => {
        setPergunta(!pergunta);
      })
      .catch((err) => {
        console.log('erro');
      });
  }

  useEffect(() => {
    const timer = setTimeout(() => getQuiz(), 2000);
    return () => clearTimeout(timer);
  }, [quiz]);

  useEffect(() => {
    const timer = setTimeout(() => getNotice(), 2000);

    return () => clearTimeout(timer);
  }, [notice]);

  useEffect(() => {
    const timer = setTimeout(() => getResponse(), 2000);

    return () => clearTimeout(timer);
  }, [resultQuiz]);

  const createEnquete = () => {
    setPergunta(!pergunta);
  };
  return (
    <>
      <S.ContainerVideo>
        <S.MidiaButtonsWrapper>
          <S.Midia>
            <iframe
              title={'video'}
              src="https://vimeo.com/event/1105610/embed/1c7d2a28d2"
              frameborder="0"
              allow="autoplay; fullscreen; picture-in-picture"
              allowfullscreen
            ></iframe>
          </S.Midia>

          <S.FaixaButtonsWrapper>
            <S.Faixa>
              <div></div>
            </S.Faixa>

            <S.ButtonsContainer>

              {notice.length > 0 ? (
                <button className="avisos">{notice[0].notice}</button>
              ) :
                <button className="avisos">MURAL DE AVISOS</button>
              }
              {
                pergunta === false ?
                  <button className="perguntas" onClick={createEnquete}>FAÇA SUA PERGUNTA</button> : (
                    <button>
                      <form onSubmit={handleSubmit}>
                        <input value={qa} onChange={ChangeQuestion} style={{ marginBottom: " 15px" }} />
                        <button style={{ width: '37%', marginTop: "0%", height: "1%", padding: "0" }}>ENVIAR</button>
                      </form>
                    </button>)

              }
            </S.ButtonsContainer>
          </S.FaixaButtonsWrapper>
        </S.MidiaButtonsWrapper>

        <S.ExitQuestionWrapper>

          {logoCondition === true ? (
            <S.LogoContainer>
              <S.Logo src={logo} />
            </S.LogoContainer>
          ) : quiz.length > 0 ? (
            <S.QuestionsContainer>
              {results}
              {quiz.map((q) => (
                <>
                  <h2 key={q.id}>{q.question}</h2>
                  <div style={{ display: "flex" }}>
                    <input type="radio" value={'a'}
                      name="a"
                      onClick={() => handleResponseAnswer('a', q.id)} />
                    <p>
                      <span>A. </span>{q.option1}
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    <input value={'b'}
                      type="radio"
                      name="b"
                      onClick={() => handleResponseAnswer('b', q.id)} />
                    <p>
                      <span>B. </span> {q.option2}
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    <input type="radio" value={'c'}
                      name="c"
                      onClick={() => handleResponseAnswer('c', q.id)} />
                    <p>
                      <span>C. </span> {q.option3}
                    </p>
                  </div>
                </>
              ))}
            </S.QuestionsContainer>
          )
            : resultsAnswer.length === [] || logoCondition === true ?
              <S.LogoContainer>
                <S.Logo src={logo} />
              </S.LogoContainer> :
              <S.LogoContainer>
                <S.Logo src={logo} />
              </S.LogoContainer>
          }
          {
            userToken === 'user' ? (
              <S.ButtonExit>
                <button onClick={handleLogout}>SAIR</button>
              </S.ButtonExit>
            ) : (
              <S.ButtonExit >
                <Link to="/painel">
                  <button>VOLTAR</button>
                </Link>
              </S.ButtonExit>
            )
          }
        </S.ExitQuestionWrapper>
      </S.ContainerVideo>
    </>
  );
}
export default VideoUser;
