import styled from 'styled-components';
import backGround from '../../assets/Tela02.jpg';
import mobile from '../../assets/mobile.jpg';

import logo800 from '../../assets/800.jpg';
import logo900 from '../../assets/1024.jpg';
import faixaMob from '../../assets/faixa-mobile.png';
import faixaDesk from '../../assets/faixa-desk.png';

export const ContainerVideo = styled.div`
  width: 100vw;
  height: 100vh;
  padding: 15px;

  @media (min-width: 780px) {
    display: flex;
    padding: 0;
    overflow: hidden;
  }
`;

export const Midia = styled.div`
  height: 35vh;
  width: 100%;
  iframe {
    width: 100%;
    height: 100%;
  }

  @media (min-width: 780px) {
    height: 100%;
    width: 100%;
    iframe {
      width: 100%;
      height: 100%;
    }
  }
`;

export const MidiaButtonsWrapper = styled.div`
  @media (min-width: 780px) {
    position: relative;
    height: 80vh;
    width: 80%;
  }
`;

export const Faixa = styled.div`
  padding-bottom: calc(180 / 1537 * 100%);
  height: 0;
  width: 100%;
  position: relative;
  div {
    background-image: url(${faixaMob});
    background-size: cover;
    background-position: center bottom;
    background-repeat: no-repeat;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
  }

  @media (min-width: 780px) {
    padding-bottom: 20vh;
    width: 100%;
    div {
      height: 100%;
      width: 100%;
      background-image: url(${faixaDesk});
      background-size: 55% 100%;
      background-position: left bottom;
    }
  }
`;

export const FaixaButtonsWrapper = styled.div`
  position: relative;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-around;
  button {
    padding: 17px;
    width: 160px;
    height: 70px;
    margin: 15px 5px;
    line-height: 20px;
    border-radius: 12px;
    color: #fff;
    background-color: #97c120;
  }

  @media (min-width: 780px) {
    width: 100%;
    height: fit-content;
    position: absolute;
    top: 0;
    bottom: 0;
    margin: auto;
    button {
      padding: 17px;
      width: 250px;
      height: 100px;
      border-radius: 15px;
    }
  }
`;

export const ExitQuestionWrapper = styled.section`
  @media (min-width: 780px) {
    margin: 10px;
    width: 30vw;
  }
`;

export const LogoContainer = styled.div`
  @media (min-width: 780px) {
    display: flex;
    align-items: center;
    height: 80vh;
  }
`;

export const Logo = styled.img`
  margin: 0 auto;
  width: 100%;
  padding: 10px;
`;

export const ButtonExit = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  button {
    color: #fff;
    padding: 5px;
    width: 100px;
    height: 40px;
    background-color: #97c120;
    border-radius: 10px;
  }

  @media (min-width: 780px) {
    height: 19vh;

    button {
      width: 130px;
      height: 60px;
    }
  }
`;

export const QuestionsContainer = styled.div`
  h2 {
    font-size: 20px;
    font-weight: 700;
    text-transform: uppercase;
    text-align: center;
    line-height: 27px;
    color: #144e92;
  }

  p {
    color: #97c120;
    text-transform: uppercase;
    margin-left: 10px;
    margin-bottom: 10px;
  }

  span {
    font-weight: 700;
    color: #144e92;
  }

  @media (min-width: 780px) {
    padding: 30px;
    border-radius: 20px;
    background-color: #97c120;
    height: 79vh;

    h2 {
      font-size: 1.5vw;
      line-height: 35px;
      margin-bottom: 30px;
    }

    p {
      font-size: 1.2vw;
      color: #fff;
      text-transform: uppercase;
    }
  }
`;
