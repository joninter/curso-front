import styled from 'styled-components';
import backGround from '../../assets/Tela02.jpg';
import mobile from '../../assets/mobile.jpg';

import logo800 from '../../assets/800.jpg';
import logo900 from '../../assets/1024.jpg';

export const CointanerVideo = styled.div`
  width: 100vw;
  padding: 15px;
`;

export const Midia = styled.div`
  height: 30%;
  iframe {
    width: 100%;
    height: 100%;
  }
`;

export const BackButton = styled.button`
  position: absolute;
  background-color: rgb(141, 198, 62);
  color: white;
  width: 124.02px;
  height: 51.81px;
  align-items: center;
  justify-content: center;
  display: flex;
  margin-top: 320px;
  float: right;
  margin-left: 950px;
  border-radius: 30px;

  //NOTEBOOK
  @media (max-width: 1366px) {
    width: 101.33px;
    height: 32.56px;
    position: absolute;
    margin: auto 0;
    margin-left: 96%;
    margin-top: 61%;
  }

  @media (max-width: 406px) {
    width: 62.17px;
    height: 3.69px;
    position: absolute;
    margin: auto 0;
    left: 234px;
    margin-top: 10px;
    border-radius: 6px;
  }
`;

export const ContainerButton = styled.div`
  position: absolute;
  display: flex;
  margin-left: 304px;
  margin-top: 58px;
  padding-top: 612px;

  //NOTEBOOK
  @media (max-width: 1366px) {
    width: 101.33px;
    height: 32.56px;
    position: absolute;
    margin: auto 0;
    margin-left: 1069px;
    margin-top: -10px;
  }

  @media (max-width: 406px) {
    width: 330px;
    height: 183.61px;
    position: absolute;
    margin: auto 0;
    margin-left: 90px;
    margin-top: 350px;
  }
`;
export const ContainerButton2 = styled.div`
  position: absolute;
  display: flex;
  margin-left: 304px;
  margin-top: -258px;
  padding-top: 612px;

  //NOTEBOOK
  @media (max-width: 1366px) {
    width: 101.33px;
    height: 32.56px;
    position: absolute;
    margin: auto 0;
    margin-left: 1069px;
    margin-top: -10px;
  }

  @media (max-width: 406px) {
    width: 330px;
    height: 183.61px;
    position: absolute;
    margin: auto 0;
    margin-left: 90px;
    margin-top: 350px;
  }
`;

export const BigButton = styled.button`
  width: 284.02px;
  height: 121.81px;
  background-color: rgb(141, 198, 62);
  border-radius: 0;
  margin-top: -34px;
  margin-left: -200px;
  margin-right: 65px;
  color: white;
  border-radius: 30px;

  //NOTEBOOK
  @media (max-width: 1366px) {
    width: 244.02px;
    height: 108.81px;
    background-color: rgb(141, 198, 62);
    border-radius: 0;
    margin-top: 31px;
    margin-left: -818px;
    color: white;
    border-radius: 30px;
  }

  @media (max-width: 406px) {
    width: 79%;
    height: 40%;
    border-radius: 6px;
  }
`;

export const BigButton2 = styled.button`
  padding-top: 12px;
  width: 284.02px;
  height: 121.81px;
  color: white;
  margin-top: -34px;
  margin-left: 260px;
  border-radius: 0;
  background-color: rgb(141, 198, 62);
  border-radius: 30px;

  //NOTEBOOK
  @media (max-width: 1366px) {
    padding-top: 12px;
    color: white;
    width: 244.02px;
    height: 108.81px;
    margin-top: 31px;
    margin-left: 41px;
    border-radius: 0;
    background-color: rgb(141, 198, 62);
    border-radius: 30px;
  }

  @media (max-width: 406px) {
    width: 80%;
    height: 30%;
    border-radius: 6px;
  }
`;

export const SmallButton = styled.div`
  position: absolute;
  padding-top: 30px;
  padding-left: 50px;
  padding-right: 10px;
  margin-left: 50.4em;
  background-color: rgb(141, 198, 62);
  border-radius: 0;
  height: 620.61px;
  width: 394.02px;
  color: white;
  margin-top: -357px;
  border-radius: 30px;

  label {
    margin-top: 80px;

    margin-right: 160px;
    width: 100%;
    height: 90px;
  }

  input {
    position: absolute;
    margin-top: 34px;
    margin-right: 9px;
    margin-left: -39px;
  }
  p {
    position: absolute;
    margin-top: 30px;
    margin-left: -20px;
    color: #1c1c1b;
  }
  div {
    margin-top: 30px;
    padding-bottom: 90px;
  }

  //NOTEBOOK
  @media (max-width: 1366px) {
    background-color: rgb(141, 198, 62);
    border-radius: 0;
    height: 622.93px;
    width: 273px;
    color: white;
    margin-top: -595%;
    margin-left: 40px;
    border-radius: 30px;
  }

  @media (max-width: 406px) {
    width: 81%;
    height: 32%;
    border-radius: 6px;
  }
`;
export const Title = styled.span`
  padding-top: 0px;
  margin-left: 90px;
  position: absolute;
  color: #1c1c1b;
`;
export const ResultButton = styled.div`
  background-color: rgb(141, 198, 62);
  border-radius: 0;
  height: 144.93px;
  width: 284.02px;
  color: white;
  margin-top: 17px;
  border-radius: 30px;

  //NOTEBOOK
  @media (max-width: 1366px) {
    background-color: rgb(141, 198, 62);
    border-radius: 0;
    height: 136.93px;
    width: 248.02px;
    color: white;
    margin-top: 17px;
    margin-left: 40px;
    border-radius: 30px;
  }

  @media (max-width: 406px) {
    width: 81%;
    height: 32%;
    border-radius: 6px;
  }
`;
export const Option = styled.div`
  color: white;
  width: 90%;
  /* height: 10%; */
  overflow-wrap: anywhere;
  padding-right: -80px;
  position: absolute;
  margin-top: 5px;
`;
export const Option1 = styled.div`
  color: white;
  width: 90%;
  /* height: 10%; */
  overflow-wrap: anywhere;
  padding-right: -80px;
  position: absolute;
  margin-top: 5px;
`;
export const Option2 = styled.div`
  color: white;
  width: 90%;
  /* height: 10%; */
  overflow-wrap: anywhere;
  padding-right: -80px;
  position: absolute;
  margin-top: 5px;
`;
export const Option3 = styled.div`
  color: white;
  width: 90%;
  /* height: 10%; */
  overflow-wrap: anywhere;
  padding-right: -80px;
  position: absolute;
  margin-top: 5px;
`;

export const Logo = styled.img`
  width: 404px;
  margin-top: -500px;
  margin-left: 1105px;

  @media (max-width: 1366px) {
    width: 280px;
    margin-top: -594px;
    margin-left: 1105px;
  }

  @media (max-width: 406px) {
    width: 330px;
    height: 183.61px;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: 45px;
  }
`;
