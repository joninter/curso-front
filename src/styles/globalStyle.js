import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

@font-face {
  font-family: 'Effra';
  src: local('Effra'), url(../assets/fonts/Effra_Std_Rg.ttf) format('truetype');
}
* {
  margin: 0;
  padding: 0;
  outline: 0;
  box-sizing: border-box;
}
html,
body,
#root {
  height: 100%;
}
body {
  background: #fff;
}
body,
input,
button {
  font-family: 'Effra', sans-serif;
}
button{
  cursor: pointer;
  background:#9159c1;
  border:0;
  padding:15px;
  border-radius:20px 20px 20px 20px;
}
`;