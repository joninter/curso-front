import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Login from './pages/Login'
import Painel from './pages/Painel'
import Controle from './pages/Controle'
import VideoAdmin from './pages/VideoAdmin'
import VideoUser from './pages/VideoUser'

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/painel" exact component={Painel} />
      <Route path="/controle" exact component={Controle} />
      <Route path="/videoadmin" exact component={VideoAdmin} />
      <Route path="/video" exact component={VideoUser} />
    </Switch>
  );
}